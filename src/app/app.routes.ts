import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { SEARCH_ROUTES } from './components/search/search.routes';


export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'search',
        component: SearchComponent,
        children: SEARCH_ROUTES
     },
    { path: '', pathMatch: 'full', redirectTo: 'home' },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
]