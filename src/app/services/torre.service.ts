import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TorreService {

  constructor( private http: HttpClient ) { }

  getProfile( params: string) {

    return this.http.get(`https://torre.bio/api/bios/${params}`).toPromise();
  }


   getBios(profile) {

    return this.http.get(`https://torre.bio/api/bios/${profile}`).toPromise();
  }

  searchOportunities(name){
    let  body={name:{term:name}}
    console.log(body);
     return this.http.post(`https://search.torre.co/opportunities/_search/?size=10&lang=es&aggregate=false&offset=0`,body).toPromise();

  }

  searchPeople(name){
   let  body={name:{term:name}}
   console.log(body);
    return this.http.post(`https://search.torre.co/people/_search/?size=10&lang=es&aggregate=false&offset=0`,body).toPromise();
  }


  searchByid(id){

    return this.http.get(`https://torre.co/api/opportunities/${id}`).toPromise();

  }

}
