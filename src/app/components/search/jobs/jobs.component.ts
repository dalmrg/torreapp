import { Component, OnInit } from '@angular/core';
import { TorreService } from 'src/app/services/torre.service';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {

  constructor( private torre: TorreService) { }
  name;
  oportunities;
  ngOnInit(): void {
  }


  async searchJobs( name) {
   this.oportunities = await this.torre.searchOportunities(this.name);
    console.log(this.oportunities);
  }

}
