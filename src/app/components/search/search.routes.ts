import { Routes } from '@angular/router';

import { PeopleComponent } from './people/people.component';
import { JobsComponent } from './jobs/jobs.component';

export const SEARCH_ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'search',
  },
  { path: 'people', component: PeopleComponent },
  { path: 'jobs', component: JobsComponent }
];
