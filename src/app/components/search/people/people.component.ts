import { Component } from '@angular/core';
import { TorreService } from 'src/app/services/torre.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent {

  persons: any;
    name;

  constructor( private torre: TorreService ) { }

 async  searchPeople( params:string) {
    this.persons = (await this.torre.searchPeople(this.name));
    console.log(this.persons);
  }



  async getProfile(username){
   console.log(await this.torre.getProfile(username));
  }
}
