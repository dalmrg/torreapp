import { Component} from '@angular/core';
import { TorreService } from '../../services/torre.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.css'
  ]
})
export class HomeComponent {

  persons: any;

  constructor( private torre: TorreService ) {



  }


   async ngOnInit() {
     this.persons = await this.torre.getProfile("dalmrg");
    console.log(this.persons);
  }


}
