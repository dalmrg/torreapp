import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noimage'
})
export class NoimagePipe implements PipeTransform {

  transform( picture: any ): string {
    
    if ( !picture ) {
      return 'assets/img/noimage.png';
    }

    if ( picture.length > 0 ) {
      return picture
    } else {
      return 'assets/img/noimage.png';
    }

  }

}
